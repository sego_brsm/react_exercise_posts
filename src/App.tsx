import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Home, PostDetails } from "./components/pages";

export const App = () => {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/post/:id">
            <PostDetails />
          </Route>
          <Route path="/" exact>
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
};

export default App;
