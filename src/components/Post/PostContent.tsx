import { Link } from "react-router-dom";
import { Post } from "../../types";

interface PostContentProps {
  post: Post;
}

const PostContent = ({ post }: PostContentProps) => {
  return (
    <div>
      <h2>{post && post.title}</h2>
      <p>{post && post.body}</p>
      <Link to="/">Retour</Link>

    </div>
  );
};

export default PostContent;
