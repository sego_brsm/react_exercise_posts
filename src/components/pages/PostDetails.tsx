import { PostContent } from "../Post";
import { useParams } from "react-router-dom";
import { useGetPost } from "../../hooks";

interface PostContentParams {
    id: string;
  }

const PostDetails = () => {
  const { id } = useParams<PostContentParams>();
  const post = useGetPost(id);
  
  return post ? <PostContent post={post}/> : null;
};

export default PostDetails;
