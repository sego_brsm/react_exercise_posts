import { useGetPostList } from "../../hooks";
import { PostList } from "../Post";

const Home = () => {
  const postList = useGetPostList();

  return <PostList posts={postList} />;
};

export default Home;
