import axios from "axios";
import { useEffect, useState } from "react";
import { Post } from "../types";

type fetchCallback = (postList: Post[]) => void;

const fetchData = async (callback: fetchCallback): Promise<void> => {
  try {
    const { data } = await axios.get<Post[]>(
      "https://jsonplaceholder.typicode.com/posts"
    );
    callback(data);
  } catch (err) {
    console.log("fetch failed", err);
  }
};

const useGetPostList = () => {
  const [postList, setPostList] = useState<Post[]>([]);

  useEffect(() => {
    fetchData((PostList) => {
      setPostList(PostList);
    });
  }, []);

  return postList;
};

export default useGetPostList;
