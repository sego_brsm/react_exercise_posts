import axios from "axios";
import { useEffect, useState } from "react";
import { Post } from "../types";

type fetchCallback = (post: Post) => void;

const fetchData = async (id: string, callback: fetchCallback) => {
  try {
    const { data } = await axios.get<Post>(
      "https://jsonplaceholder.typicode.com/posts/" + id
    );
    callback(data);
  } catch (err) {
    console.log("fetch failed", err);
  }
};


const useGetPost = (id: string) => {
    const [post, setPost] = useState<Post>();

    useEffect(() => {
        fetchData(id, (Post) => {
        setPost(Post);
        });
    }, [id]);

    return post
}

export default useGetPost