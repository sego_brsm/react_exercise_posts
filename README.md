# Exercice d'entraînement à React

### Sujet

- Récupérer la liste des posts de l'API [JSON Placeholder](https://jsonplaceholder.typicode.com/) via Axios
- Afficher la liste sur une page web
- Lorsque l'on clique sur l'un des posts, l'utilisateur est redirigé (via React Router) vers une page qui présente
  - Le texte du post
  - Les commentaires associés
  - Une flèche en haut à gauche et à droite redirigeant vers les posts précédents et suivants (pas de flèche pour les articles aux extrêmités de la liste)
- Sur la page principale doit se trouver un bouton "Actualiser" relançant une requête de récupération de posts

### Contraintes

- Faire le projet en Typescript
- Ne pas utiliser .then(), .catch() et etc... pour récupérer le résultat d'une Promise
- Faire les appels via Axios dans des fonctions à part
- Utiliser les composants via des exports default
- Utiliser des custom hook au lieu de useState, useEffect, ...

### Configuration

- Ecrire un fichier eslintrc et prettierrc